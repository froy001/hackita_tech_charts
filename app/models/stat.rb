class Stat
  include Mongoid::Document

  field :csharp, type: String
  field :linux, type: String
  field :git, type: String
  field :java, type: String
  field :dcvs, type: String
  field :rails, type: String
  field :perl, type: String
  field :non_relational_databases, type: String
  field :html, type: String
  field :css, type: String
  field :python, type: String
  field :javascript, type: String
  field :relational_databases, type: String
  field :mac, type: String
  field :php, type: String
  field :ruby, type: String
  field :c, type: String
  field :windows, type: String
  field :django, type: String
  field :old_cvs, type: String
  field :cpp, type: String
  field :years_of_experience, type: String
  field :functional_languages, type: String
  field :cs_education, type: String
end
