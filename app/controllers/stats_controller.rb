class StatsController < ApplicationController

  def index
    keys = Stat.fields.keys
    @datas = []
    data = []

    keys.each do |key|
      (0..10).each do |n|
        pair = []
        pair << n.to_s
        count = Stat.where(:"#{key}" => n).count
        pair << count
        data << pair
        pair = []
      end
      hash = { name: "#{key}", data: data}
      data = []
      @datas << hash
      hash = {}
    end
  end

end
